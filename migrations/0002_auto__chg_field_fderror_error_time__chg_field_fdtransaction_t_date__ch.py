# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'FDError.error_time'
        db.alter_column('django_firstdata_fderror', 'error_time', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'FDTransaction.t_date'
        db.alter_column('django_firstdata_fdtransaction', 't_date', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'FDBatch.close_date'
        db.alter_column('django_firstdata_fdbatch', 'close_date', self.gf('django.db.models.fields.DateTimeField')())
    def backwards(self, orm):

        # Changing field 'FDError.error_time'
        db.alter_column('django_firstdata_fderror', 'error_time', self.gf('django.db.models.fields.CharField')(max_length=20))

        # Changing field 'FDTransaction.t_date'
        db.alter_column('django_firstdata_fdtransaction', 't_date', self.gf('django.db.models.fields.CharField')(max_length=20))

        # Changing field 'FDBatch.close_date'
        db.alter_column('django_firstdata_fdbatch', 'close_date', self.gf('django.db.models.fields.CharField')(max_length=20))
    models = {
        'django_firstdata.fdbatch': {
            'Meta': {'object_name': 'FDBatch'},
            'amount_reversal': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'amount_transaction': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'close_date': ('django.db.models.fields.DateTimeField', [], {}),
            'count_reversal': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'count_transaction': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'response': ('django.db.models.fields.TextField', [], {}),
            'result': ('django.db.models.fields.TextField', [], {}),
            'result_code': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'django_firstdata.fderror': {
            'Meta': {'object_name': 'FDError'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'error_time': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'response': ('django.db.models.fields.TextField', [], {})
        },
        'django_firstdata.fdtransaction': {
            'Meta': {'object_name': 'FDTransaction'},
            'amount': ('django.db.models.fields.IntegerField', [], {}),
            'card_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'client_ip_addr': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'currency': ('django.db.models.fields.IntegerField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'dms_ok': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'makeDMS_amount': ('django.db.models.fields.IntegerField', [], {}),
            'response': ('django.db.models.fields.TextField', [], {}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'result_3dsecure': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'result_code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'reversal_amount': ('django.db.models.fields.IntegerField', [], {}),
            't_date': ('django.db.models.fields.DateTimeField', [], {}),
            'trans_id': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['django_firstdata']