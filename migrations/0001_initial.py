# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FDTransaction'
        db.create_table('django_firstdata_fdtransaction', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('trans_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('amount', self.gf('django.db.models.fields.IntegerField')()),
            ('currency', self.gf('django.db.models.fields.IntegerField')()),
            ('client_ip_addr', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('language', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('dms_ok', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('result', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('result_code', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('result_3dsecure', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('card_number', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('t_date', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('response', self.gf('django.db.models.fields.TextField')()),
            ('reversal_amount', self.gf('django.db.models.fields.IntegerField')()),
            ('makeDMS_amount', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('django_firstdata', ['FDTransaction'])

        # Adding model 'FDBatch'
        db.create_table('django_firstdata_fdbatch', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('result', self.gf('django.db.models.fields.TextField')()),
            ('result_code', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('count_reversal', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('count_transaction', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('amount_reversal', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('amount_transaction', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('close_date', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('response', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('django_firstdata', ['FDBatch'])

        # Adding model 'FDError'
        db.create_table('django_firstdata_fderror', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('error_time', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('action', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('response', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('django_firstdata', ['FDError'])

    def backwards(self, orm):
        # Deleting model 'FDTransaction'
        db.delete_table('django_firstdata_fdtransaction')

        # Deleting model 'FDBatch'
        db.delete_table('django_firstdata_fdbatch')

        # Deleting model 'FDError'
        db.delete_table('django_firstdata_fderror')

    models = {
        'django_firstdata.fdbatch': {
            'Meta': {'object_name': 'FDBatch'},
            'amount_reversal': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'amount_transaction': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'close_date': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'count_reversal': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'count_transaction': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'response': ('django.db.models.fields.TextField', [], {}),
            'result': ('django.db.models.fields.TextField', [], {}),
            'result_code': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'django_firstdata.fderror': {
            'Meta': {'object_name': 'FDError'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'error_time': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'response': ('django.db.models.fields.TextField', [], {})
        },
        'django_firstdata.fdtransaction': {
            'Meta': {'object_name': 'FDTransaction'},
            'amount': ('django.db.models.fields.IntegerField', [], {}),
            'card_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'client_ip_addr': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'currency': ('django.db.models.fields.IntegerField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'dms_ok': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'makeDMS_amount': ('django.db.models.fields.IntegerField', [], {}),
            'response': ('django.db.models.fields.TextField', [], {}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'result_3dsecure': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'result_code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'reversal_amount': ('django.db.models.fields.IntegerField', [], {}),
            't_date': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'trans_id': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['django_firstdata']