import datetime
from django.conf.urls import patterns
from django.contrib import admin, messages
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from .models import *

class FDTransactionAdmin(admin.ModelAdmin):
    model = FDTransaction
    change_form_template = 'firstdata/admin/change_form.html'
    actions = None

    readonly_fields = ['trans_id','amount','currency',
                       'client_ip_addr','description',
                       'language','dms_ok','result',
                       'result_code','result_3dsecure',
                       'card_number','t_date','response',
                       'reversal_amount','makeDMS_amount']

    list_display = ['trans_id','t_date','amount','currency','result','result_code','result_3dsecure','reversal_amount']

    search_fields = ['trans_id','card_number']

    date_hierarchy = 't_date'

    def get_urls(self):
        urls = super(FDTransactionAdmin, self).get_urls()
        my_urls = patterns('',
            (r'^(?P<transaction_id>[0-9]+)/reverse/$', self.my_view)
        )
        return my_urls + urls

    def my_view(self, request, transaction_id = None):
        if transaction_id is None:
            raise Http404

        object = self.get_object(request,transaction_id)

        from .merchant import Merchant

        if request.method == 'POST':
            if 'reverse_amount' in request.POST:
                reverse_amount = int(request.POST['reverse_amount'])

                merchant = Merchant()

                data = merchant.reverse(object.trans_id,reverse_amount)

                data_array = data.split("\n")

                data_dict = {}

                for item in data_array:
                    item_array = item.split(":")
                    data_dict[item_array[0]] = item_array[1][1:]

                if 'RESULT' in data_dict:
                    result = data_dict['RESULT']

                    if result == 'OK' or result == 'REVERSED':
                        object.reversal_amount = reverse_amount
                        object.result_code = data_dict['RESULT_CODE']
                        object.response = data
                        object.save()

                        messages.success(request,'Transaction reversal successful!')
                        return HttpResponseRedirect(reverse('admin:django_firstdata_fdtransaction_change',args=(object.id,)))

                    else:
                        err = FDError()
                        err.error_time = datetime.datetime.now()
                        err.action = 'reverse'
                        err.response = data
                        err.save()

                        messages.error(request,'Transaction reversal error!')
                        return HttpResponseRedirect(reverse('admin:django_firstdata_fdtransaction_change',args=(object.id,)))

                return HttpResponse(data)

            else:
                raise Http404

        return render_to_response('firstdata/admin/reverse_dialog.html',{'object':object},context_instance=RequestContext(request))

#        merchant = Merchant()
#
        pass


admin.site.register(FDTransaction,FDTransactionAdmin)

class FDErrorAdmin(admin.ModelAdmin):
    readonly_fields = ['error_time','action','response']
    actions = None

    date_hierarchy = 'error_time'

admin.site.register(FDError)

class FDBatchAdmin(admin.ModelAdmin):
    readonly_fields = ['result','result_code','count_reversal',
                       'count_transaction','amount_reversal',
                       'amount_transaction','close_date',
                       'response']

    list_display = ['result','result_code','count_reversal',
                    'count_transaction','amount_reversal',
                    'amount_transaction','close_date',]

    actions = None

    date_hierarchy = 'close_date'

admin.site.register(FDBatch,FDBatchAdmin)