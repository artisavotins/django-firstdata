from django.db import models

class FDTransaction(models.Model):
    trans_id = models.CharField(max_length=50)
    amount = models.IntegerField()
    currency = models.IntegerField()
    client_ip_addr = models.CharField(max_length=50)
    description = models.TextField()
    language = models.CharField(max_length=50)
    dms_ok = models.CharField(max_length=50)
    result = models.CharField(max_length=50)
    result_code = models.CharField(max_length=50)
    result_3dsecure = models.CharField(max_length=50)
    card_number = models.CharField(max_length=50)
    t_date = models.DateTimeField()
    response = models.TextField()
    reversal_amount = models.IntegerField()
    makeDMS_amount = models.IntegerField()

    def __unicode__(self):
        return u'TRANS_ID(%s) %s %s (%s)' % (self.trans_id,self.result,self.result_code,self.t_date)

    class Meta:
        ordering = ['-id']

class FDBatch(models.Model):
    result = models.TextField()
    result_code = models.CharField(max_length=3)
    count_reversal = models.CharField(max_length=10)
    count_transaction = models.CharField(max_length=10)
    amount_reversal = models.CharField(max_length=16)
    amount_transaction = models.CharField(max_length=16)
    close_date = models.DateTimeField()
    response = models.TextField()

    class Meta:
        ordering = ['-id']

class FDError(models.Model):
    error_time = models.DateTimeField()
    action = models.CharField(max_length=20)
    response = models.TextField()

    def __unicode__(self):
        return u'%s %s %s' % (self.error_time,self.action,self.response)

    class Meta:
        ordering = ['-id']