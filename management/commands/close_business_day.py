import datetime
from django.core.management.base import BaseCommand
from django_firstdata.models import FDBatch,FDError

class Command(BaseCommand):

    help = 'Close business day'

    def handle(self,*args,**options):
        from django_firstdata.merchant import Merchant

        merchant = Merchant()

        data = merchant.closeDay()

        data_array = data.split("\n")

        data_dict = {}

        for item in data_array:
            item_array = item.split(":")
            data_dict[item_array[0]] = item_array[1][1:]

        if 'RESULT' in data_dict:
            try:
                batch = FDBatch()
                batch.result = data_dict['RESULT']
                batch.result_code = data_dict['RESULT_CODE']
                try:
                    batch.count_reversal = data_dict['FLD_075']
                except KeyError:
                    batch.count_reversal = ''

                try:
                    batch.count_transaction = data_dict['FLD_076']
                except KeyError:
                    batch.count_transaction = ''

                try:
                    batch.amount_reversal = data_dict['FLD_087']
                except KeyError:
                    batch.amount_reversal = ''

                try:
                    batch.amount_transaction = data_dict['FLD_088']
                except KeyError:
                    batch.amount_transaction = ''

                batch.response = data
                batch.close_date = datetime.datetime.now()

                batch.save()

                print "OK"

            except KeyError:
                error = FDError()
                error.error_time = datetime.datetime.now()
                error.action = 'closeDay'
                error.response = data
                error.save()

                print "ERROR"
        else:
            error = FDError()
            error.error_time = datetime.datetime.now()
            error.action = 'closeDay'
            error.response = data
            error.save()

            print "ERROR"