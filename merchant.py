import requests

from django.conf import settings as user_settings


FIRSTDATA_DEBUG = getattr(user_settings, 'FIRSTDATA_DEBUG', True)  # falling back to debug/sandbox environment is safer

FIRSTDATA_DEVELOPMENT_SERVER_URL = getattr(user_settings, 'FIRSTDATA_DEVELOPMENT_SERVER_URL',
                                           'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler')
FIRSTDATA_DEVELOPMENT_CLIENT_URL = getattr(user_settings, 'FIRSTDATA_DEVELOPMENT_CLIENT_URL',
                                           'https://secureshop-test.firstdata.lv/ecomm/ClientHandler')

FIRSTDATA_PRODUCTION_SERVER_URL = getattr(user_settings, 'FIRSTDATA_PRODUCTION_SERVER_URL',
                                          'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler')
FIRSTDATA_PRODUCTION_CLIENT_URL = getattr(user_settings, 'FIRSTDATA_PRODUCTION_CLIENT_URL',
                                          'https://secureshop.firstdata.lv/ecomm/ClientHandler')

FIRSTDATA_CERTIFICATE_FILE = getattr(user_settings, 'FIRSTDATA_CERTIFICATE_FILE', None)
FIRSTDATA_CERTIFICATE_PLAIN_FILE = getattr(user_settings, 'FIRSTDATA_CERTIFICATE_PLAIN_FILE', None)
FIRSTDATA_CERTIFICATE_PASS = getattr(user_settings, 'FIRSTDATA_CERTIFICATE_PASS', '')

FIRSTDATA_DEFAULT_CURRENCY = getattr(user_settings, 'FIRSTDATA_DEFAULT_CURRENCY',
                                     978)  # 428=LVL 978=EUR 840=USD 941=RSD 703=SKK 440=LTL 233=EEK 643=RUB 891=YUM


class Merchant(object):
    server_url = ''
    client_url = ''

    keystore = None
    keystore_plain = None

    verbose = True

    data = ''

    def __init__(self, verbose=True, keystore=None, keystore_plain=None):
        if FIRSTDATA_DEBUG:
            self.server_url = FIRSTDATA_DEVELOPMENT_SERVER_URL
            self.client_url = FIRSTDATA_DEVELOPMENT_CLIENT_URL
        else:
            self.server_url = FIRSTDATA_PRODUCTION_SERVER_URL
            self.client_url = FIRSTDATA_PRODUCTION_CLIENT_URL

        if keystore is None:
            self.keystore = FIRSTDATA_CERTIFICATE_FILE
        else:
            self.keystore = keystore

        if keystore_plain is None:
            self.keystore_plain = FIRSTDATA_CERTIFICATE_PLAIN_FILE
        else:
            self.keystore_plain = keystore_plain

        self.verbose = verbose

        self.data = ''

    def _send_post(self, data):
        response = requests.post(
            self.server_url,
            data,
            cert=(
                self.keystore,
                self.keystore_plain
            ),
            verify=False,
            timeout=(10, 10)
        )

        return response.content

    def startSMSTrans(self, amount, currency, ip, desc, language):
        data = {
            'command': 'v',
            'amount': str(amount),
            'currency': str(currency),
            'client_ip_addr': ip,
            'description': desc,
            'language': language
        }

        return self._send_post(data)

    def startDMSTrans(self, amount, currency, ip, desc, language):
        data = {
            'command': 'v',
            'msg_type': 'DMS',
            'amount': str(amount),
            'currency': str(currency),
            'client_ip_addr': ip,
            'description': desc,
            'language': language
        }

        return self._send_post(data)

    def makeDMSTrans(self, auth_id, amount, currency, ip, desc, language):
        data = {
            'command': 't',
            'msg_type': 'DMS',
            'trans_id': auth_id,
            'amount': amount,
            'currency': currency,
            'client_ip_addr': ip,
        }

        return self._send_post(data)

    def getTransResult(self, trans_id, ip):
        data = {
            'command': 'c',
            'trans_id': trans_id,
            'client_ip_addr': ip
        }

        return self._send_post(data)

    def reverse(self, trans_id, amount):
        data = {
            'command': 'r',
            'trans_id': trans_id,
            'amount': amount
        }

        return self._send_post(data)

    def closeDay(self):
        data = {
            'command': 'b',
        }

        return self._send_post(data)

    @staticmethod
    def get_transaction_id(data):
        if 'TRANSACTION_ID: ' in data:
            return data[16:]
        else:
            raise Exception("No transaction ID in response!")

    def get_redirect_url(self):
        if FIRSTDATA_DEBUG:
            return FIRSTDATA_DEVELOPMENT_CLIENT_URL
        else:
            return FIRSTDATA_PRODUCTION_CLIENT_URL
