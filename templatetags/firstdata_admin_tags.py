from django import template

register = template.Library()

@register.inclusion_tag('firstdata/admin/submit_line.html', takes_context=True)
def fd_submit_row(context):
    """
    Displays the row of buttons for delete and save.
    """
    opts = context['opts']
    object = context['original']

    show_reverse_link = False
    if object.result == 'OK':
        show_reverse_link = True

    change = context['change']
    is_popup = context['is_popup']
    save_as = context['save_as']
    return {
        'onclick_attrib': (opts.get_ordered_objects() and change
                           and 'onclick="submitOrderForm();"' or ''),
        'show_delete_link': False,
        'show_save_as_new': False,
        'show_save_and_add_another': False,
        'show_save_and_continue': not is_popup and context['has_change_permission'],
        'is_popup': is_popup,
        'show_save': True,
        'show_reverse_link':show_reverse_link,
    }
